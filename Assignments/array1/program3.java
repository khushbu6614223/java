//WAP to take size of array from user and also take integer elements from user print product of odd index only


import java.io.*;
class PindexDemo{

        public static void main(String[] args)throws IOException{
                BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
                System.out.println("Enter the size of Array:");
                int size=Integer.parseInt(br.readLine());
                int arr[]=new int[size];

                System.out.println("Enter the Elements of Array:");
                for(int i=0;i<arr.length;i++){
                        arr[i]=Integer.parseInt(br.readLine());
                }
                int product=1;
                for(int i=0;i<arr.length;i++){
                        if(i%2==1){
                                 product=product*arr[i];
                        }
                }
                System.out.println("Product of odd index Elements:" +product);


        }

}
