//WAP, take 10 input from the user & print only the numbers divisible by 5
//using bufferedreader

import java.io.*;
class Demo{

        public static void main(String[] args)throws IOException{

                BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
                System.out.println("Enter the size of Array:");
                int size=Integer.parseInt(br.readLine());
                int arr[]= new int[size];

                System.out.println("Enter the Elements of the Array:");

                for(int i=0;i<arr.length;i++){
                        arr[i]= Integer.parseInt(br.readLine());
                }
		System.out.println("The Elements divisible by 5 in the array  are:");
                for(int i=0;i<arr.length;i++){
			if(arr[i]%5==0){
				System.out.println(arr[i]);
			}
		}

	}
}

