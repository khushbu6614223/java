//WAP to to create an array of 'n' integer elements where 'n' values should be taken from user 
//Insert the values from users and find the sum of all elements in the array

import java.io.*;
class ArrayDemo{

	public static void main(String args[])throws IOException{

		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter the size of Array:");
		int size=Integer.parseInt(br.readLine());

		int arr[] = new int[size];

		System.out.println("Enter the Elements:");
		int sum=0;
		for(int i=0; i<arr.length; i++){
			arr[i]=Integer.parseInt(br.readLine());
			sum=sum+arr[i];
		}
		System.out.println("Sum of all elements in array  is:" +sum);

	}

}
