//WAP to print the elements whose addition of digits is even


import java.io.*;
class Demo{

        public static void main(String args[])throws IOException{

                BufferedReader br=new BufferedReader(new InputStreamReader(System.in));

                System.out.println("Enter the size of Array:");
                int size=Integer.parseInt(br.readLine());
                int arr[] = new int[size];

                System.out.println("Enter the Elements in the array:");
                 for(int i=0; i<arr.length; i++){
                        arr[i]=Integer.parseInt(br.readLine());
                 }
		
		 int sum=0;
		 System.out.println("Output:");
		 for(int i=0; i<arr.length; i++){
			 
			int num=arr[i];
			while(num!=0){
			 int rem=num%10;
			 sum=sum+rem;
			 num=num/10;
			}
			 if(sum%2==0){
				 System.out.println(arr[i]);
			 }
				sum=0;
		 
		 }
	
	}
}
