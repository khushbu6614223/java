//WAP to find the number of even and odd integers in a given array of integers

import java.io.*;
class ArrayDemo{

        public static void main(String args[])throws IOException{

                BufferedReader br=new BufferedReader(new InputStreamReader(System.in));

                System.out.println("Enter the size of Array:");
                int size=Integer.parseInt(br.readLine());

                int arr[] = new int[size];

                System.out.println("Enter the Elements:");
                for(int i=0; i<arr.length; i++){
                        arr[i]=Integer.parseInt(br.readLine());
		}
		int Evencount=0,Oddcount=0;
		for(int i=0; i<arr.length; i++){
			if(arr[i]%2==0){
				Evencount++;
			}
			else{
				Oddcount++;
	     			}
		}
		System.out.println("Number of even count=" +Evencount);
		System.out.println("Number of odd count=" +Oddcount);
	}
}

