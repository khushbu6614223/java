//WAP to find the sum of even & odd numbers in a array

import java.io.*;
class ArrayDemo{

        public static void main(String args[])throws IOException{

                BufferedReader br=new BufferedReader(new InputStreamReader(System.in));

                System.out.println("Enter the size of Array:");
                int size=Integer.parseInt(br.readLine());

                int arr[] = new int[size];

                System.out.println("Enter the Elements:");
                int esum=0;
		int osum=0;
                for(int i=0; i<arr.length; i++){
                        arr[i]=Integer.parseInt(br.readLine());

			if(arr[i]%2==0)
				esum=esum+arr[i];
			else
				osum=osum+arr[i];
		}
		System.out.println("sum of even numbers=" +esum);
		System.out.println("sum of odd numbers=" +osum);
	}
}
