//WAP to search a specific element from an array and return its index value


import java.io.*;
class ArrayDemo{

        public static void main(String args[])throws IOException{

                BufferedReader br=new BufferedReader(new InputStreamReader(System.in));

                System.out.println("Enter the size of Array:");
                int size=Integer.parseInt(br.readLine());

                int arr[] = new int[size];

                System.out.println("Enter the Elements:");
                for(int i=0; i<arr.length; i++){
                        arr[i]=Integer.parseInt(br.readLine());
		}

		System.out.println("Enter element to search:");
		int search=Integer.parseInt(br.readLine());

		for(int i=0; i<arr.length; i++){
			if(arr[i]== search){
				System.out.println("Element found at index: " +i);
			}
		}
	}
}

