//WAP to create an array of 'n' integer elements where 'n' values should be taken from user
//find the minimum element from array

import java.io.*;
class ArrayDemo{

        public static void main(String args[])throws IOException{

                BufferedReader br=new BufferedReader(new InputStreamReader(System.in));

                System.out.println("Enter the size of Array:");
                int size=Integer.parseInt(br.readLine());

                int arr[] = new int[size];

                System.out.println("Enter the Elements:");
               
                for(int i=0; i<arr.length; i++){
                        arr[i]=Integer.parseInt(br.readLine());
		}
		int min=arr[0];
		
		for(int i=0; i<arr.length; i++){
			if(arr[i]<min){

				min=arr[i];
			}
		
		}

				System.out.println("Minimum element in the array: " + min);
		}
}
