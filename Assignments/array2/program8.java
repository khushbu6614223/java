//WAP to find the uncommon elements from the array

import java.io.*;
class Demo{

        public static void main(String args[])throws IOException{

                BufferedReader br=new BufferedReader(new InputStreamReader(System.in));

                System.out.println("Enter the size of Array-1:");
                int size1=Integer.parseInt(br.readLine());
                int arr1[] = new int[size1];

                System.out.println("Enter the Elements in the array-1:");
                 for(int i=0; i<arr1.length; i++){
                        arr1[i]=Integer.parseInt(br.readLine());
                 }

                System.out.println("Enter the size of Array-2:");
                int size2=Integer.parseInt(br.readLine());
                int arr2[] = new int[size2];

                System.out.println("Enter the Elements in the array-2:");
                 for(int k=0; k<arr2.length; k++){
                        arr2[k]=Integer.parseInt(br.readLine());
                 }

                 System.out.println("Uncommon elements between arrays are: ");
                 for(int i=0; i<arr1.length; i++){
                         for(int k=0; k<arr2.length; k++){
                                 if(arr1[i]!=arr2[k]){
                                         System.out.println(arr1[i]);
				 }
                        	
			 }
		 }
	}
}
