//Write a unique real-time example  of If ElseIf Else Ladder


class Demo{

	public static void main(String[] args){
	
		float money = 2500.00f;

		if(money <= 0.00f){
		
			System.out.println("Don't go anywhere");
		}

		else if(money >= 800f){
		
			System.out.println("Movie at Theatre");
	
		}else if(money >= 2000f){
		
			System.out.println("Shopping at Mall");
	
		}else if(money >= 3000f){
		
			System.out.println("Dine in 7 star");
		}else{
		
			System.out.println("PG");
		}


	}
}
