//WAP that takes the cost price and selling price(hardcoded)and calculates its profit or loss


class Demo{

	public static void main(String[] args){
	
		int SellingPrice = 1200;
		int CostPrice = 1000;

		if(SellingPrice > CostPrice){
		
			System.out.println("Profit of "+(SellingPrice-CostPrice));
		}else if(SellingPrice < CostPrice){

			System.out.println("Loss of "+(CostPrice-SellingPrice));
		}else{

			System.out.println("No Loss No Profit");
		} 

	}
}
