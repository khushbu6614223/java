/*WAP to print
F 5 D 3 B 1
F 5 D 3 B 1
F 5 D 3 B 1
F 5 D 3 B 1
F 5 D 3 B 1
F 5 D 3 B 1
*/

class PatternDemo1{

	public static void main(String args[]){
	
		int rows = 6;

		for(int i=1; i<=rows; i++){
			char ch = 'F';
			int num = rows;

			for(int j=1; j<=rows; j++){
				if(j%2==1){
					System.out.print(ch+ " ");
				}
				else{
					System.out.print(num + " ");
				}
				ch--;
				num--;
			}
			System.out.println();
		}
	}
}
