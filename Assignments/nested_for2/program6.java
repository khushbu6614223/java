/*WAP to print
1 4 9 
16 25 36
49 64 81
*/

class PatternDemo1{

	public static void main(String[] args){
	
		int num = 1;

		for(int i=1; i<=3; i++){

			for(int j=1; j<=3; j++){
			
				int square = num*num;
				System.out.print(square+ " ");
				num++;
			}

			System.out.println();
		}
	}
}
