/*WAP to print
1C3 4B2 9A1
16C3 25B2 36A1
49C3 64B2 81A1
*/

class PatternDemo1{

	public static void main(String[] args){
	
		int rows = 3;
		int x = 1;

		for(int i=1; i<=rows; i++){
			char ch = 'C';
			int num = rows;

			for(int j=1; j<=rows; j++){

				System.out.print(x*x + "" +ch+ "" +num-- +" ");
				x++;
				ch--;
			}
			System.out.println();
		}

	}	
}
