/*WAP to print
1 2 3 4
4 5 6
6 7
7
*/


class PatternDemo2{

        public static void main(String[] args){

                int rows = 4;
                int num = 1;

                for(int i=1; i<=rows; i++){
                        for(int j=1; j<=(rows+1-i); j++){

                                System.out.print(num+ " ");
                                num++;
                        }
			num--;
                        System.out.println();
                }
        }
}
