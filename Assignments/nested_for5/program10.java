/*WAP to print series of prime number from entered range(take a start and end number from user)*/ 

import java.io.*;
class Demo{

	public static void main(String[] args)throws IOException{
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter start and end of range");
		int start=Integer.parseInt(br.readLine());
		int end=Integer.parseInt(br.readLine());

		System.out.println("Prime numbers series:");

		for(int i=start;i<=end;i++){
			int count=0;
			for(int j=1;j*j<i;j++){
				if(i%j==0)
					count=count+2;
			
				if(count>2)
					break;
			}
				if(count<=2&&i!=1)
					 System.out.print(i+" ");
				
		}
				 System.out.println();
			
		


	}

}
