/*WAP to print all even numbers in reverse order and odd numbers in the standartd way. Both seperately. Within a range. Take the start and end from user*/


import java.io.*;
class Demo{

	public static void main(String[] args)throws IOException{

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter start and end number:");

		int start=Integer.parseInt(br.readLine());
		int end=Integer.parseInt(br.readLine());
		
			 System.out.println("Even numbers:");
		        	  for(int i=end;i>=start;i--){
					if(i%2==0)
					 System.out.print(i+ " ");
					}

			System.out.println();	  

		     	System.out.println("odd numbers:");
			
				for(int i=start;i<=end;i++){	
					if(i%2==1)	
     			     		System.out.print(i+ " ");
				}
			System.out.println();
		
		}

}
