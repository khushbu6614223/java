/*WAP to print 
  0 
  1 1
  2 3 5
  8 13 21 34
  */

import java.io.*;
class Demo{

	public static void main( String args[])throws IOException{

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter row:");
		int row=Integer.parseInt(br.readLine());

		int a=0;
		int b=1;
		for(int j=1;j<=row;j++){

			for(int x=1;x<=j;x++){

				System.out.print(a +" " );

				int c=a+b;
				a=b;
				b=c;
			}
			System.out.println();
		}
	}

}

