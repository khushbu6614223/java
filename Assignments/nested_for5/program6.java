/*WAP and take 2 char if these char are equal then print them as it is but if they are unequal then print their difference*/

import java.io.*;
class Demo{

	public static void main(String[] args)throws IOException{
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter two characters:");
		char ch1=(char)br.read();
		br.skip(1);
		char ch2=(char)br.read();

		if(ch1==ch2)
			System.out.println("Two characters are same");
		else{
			int diff=ch1-ch2;
		if(diff<0){
			diff=diff*(-1);
		}
		System.out.println("The difference between" +ch1 + "&" +ch2 + "characters is: " +diff);
		
		}
	}

}

