/*WAP to take a number as I/p and print the addition of factorilas of each digit from that number*/

import java.io.*;
class Demo{

        public static void main(String args[])throws IOException{

                BufferedReader br= new BufferedReader(new InputStreamReader(System.in));
                System.out.println("Enter number:");
                int num=Integer.parseInt(br.readLine());
		int t=0;
                while(num!=0){
			int rem=num%10;
			int fact=1;
			while(rem!=0){
				fact=fact*rem;
				rem--;
			}
			t=t+fact;
			num=num/10;
		}
		System.out.println(t);
	}
}
