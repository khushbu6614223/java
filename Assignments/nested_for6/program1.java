//WAP to print the number divisible by 5 from 1 to 50 & the number is even also print the count of even numbers

import java.io.*;
class Demo{

	public static void main(String[] args)throws IOException{
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter the start:");
		int start=Integer.parseInt(br.readLine());

		System.out.println("Enter the End:");
		int End=Integer.parseInt(br.readLine());

		int count=0;

		for(int i=start; i<=End; i++){
			if(i%5==0 && i%2==0){
				count++;
			
			System.out.println(i+ " ");
			}
		}
		System.out.println("Count=" +count);
	}
}
