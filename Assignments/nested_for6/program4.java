//WAP to take a range as input from user and print perfect cubes between that range


import java.io.*;
class Demo{

	public static void main(String[] args)throws IOException{
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter start:");
		int start = Integer.parseInt(br.readLine());
		
                System.out.println("Enter end:");
                int end = Integer.parseInt(br.readLine());

		for(int i=start; i<=end; i++){
			for(int j=1; j<=i; j++){
				int cube = j*j*j;
				if(cube == i)
					System.out.println(cube);
			}
		}
			
	}
}
