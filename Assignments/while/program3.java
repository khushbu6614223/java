//WAP to count the digits of given number


class Demo{

	public static void main(String[] args){
	
		int N = 95294210;
		int count = 0;

		while(N!=0){
		
			count++;
			N = N/10;
		}
		System.out.println(count);

	}
}
