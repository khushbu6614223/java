//WAP to print the sum of all even numbers and the multiplication of odd numbers between 1 to 10


class Demo{

	public static void main(String args[]){
	
		int N = 10;
		int i = 1;
	
		int sum = 0;
		int mult = 1;

		while(i<=N){
		
			if(i%2==0){
				sum = sum+i;
				i++;
			}
			else{
				mult = mult*i;
				i++;
			}
		}
		
		System.out.println("Sum of even number= " +sum);
                System.out.println("multiplication of odd number= " +mult);
	}
}
