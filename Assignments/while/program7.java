//WAP that takes a number, if the number is even print that number in reverse order, or if the number is odd print that number in reverse order by difference two


class Demo{

	public static void main(String[] args){
	
		int N = 6;
		if(N%2==0){
			
			while(N>=1){
				System.out.println(N + " ");
				N--;
			}
		}
		else{
			while(N>=1){
				System.out.println(N + " ");
				N = N-2;
			}
		}
		System.out.println();

	}
}
