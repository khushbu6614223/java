//WAP to reverse the given number


class Demo{

	public static void main(String[] args){
	
		int num = 942111423;
		int temp = num;
		int rev = 0;

		while(num!=0){
			int rem = num%10;
			rev = rev*10 + rem;
			num = num/10;
		}
		System.out.println(rev);
	}
}
