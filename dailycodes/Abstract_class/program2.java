//Real Time Example for abtract class


abstract class RBI{

	void rule(){
	
		System.out.println("Currency Type");
	}
	abstract void InterestRate();
}
class IDBI extends RBI{

	void InterestRate(){
	
		System.out.println("5.9%"); 
	}
}
class Client{

	public static void main(String[] args){
	
		IDBI obj = new IDBI();
		obj.rule();
		obj.InterestRate();
	}
}
