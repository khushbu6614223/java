//Set
//HashSet


import java.util.*;

class HashSetDemo {

	public static void main(String[] args) {
	
		HashSet hs = new HashSet();

		hs.add("Kanha");
		hs.add("Rahul");
		hs.add("Ashish");
		hs.add("Badhe");
		hs.add("Kanha");
	
		System.out.println(hs);

		System.out.println(hs.add("shashi"));
		System.out.println(hs.add("Ashish"));
		System.out.println(hs.add("Rahul"));

		System.out.println(hs);
	}
}
