//TreeSet (user-define)
//Comparable

import java.util.*;

class Movies implements Comparable{

	String MovieName = null;
	float totColl = 0.0f;

	Movies(String MovieName ,float totColl) {
	
		this.MovieName = MovieName;
		this.totColl = totColl;
	}

	public int compareTo(Object obj) {
	
		return -(MovieName.compareTo(((Movies)obj).MovieName));
	}

	public String toString() {
	
		return "{" + MovieName + "=" + totColl + "}";
	}
}
class TreeSetDemo {

	public static void main(String[] args) {
	
		TreeSet t = new TreeSet();

		t.add(new Movies("Gadar2",150.00f));
		t.add(new Movies("OMG2",120.0f));
		t.add(new Movies("Jailer",250.0f));
		t.add(new Movies("Gadar2",150.0f));

		System.out.println(t);
	}
}
