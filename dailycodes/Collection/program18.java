//Comparator on list


import java.util.*;

class Movies { 

	String Name = null;
	double Coll = 0.0;
	float imdbRating = 0.0f;

	Movies(String Name, double Coll, float imdbRating) {
	
		this.Name = Name;
		this.Coll = Coll;
		this.imdbRating = imdbRating;
	}
	public String toString() {
	
		return "{" + Name + ";" + Coll + ";" + imdbRating + "}";
	}
}

class SortByName implements Comparator {

	public int compare(Object obj1, Object obj2) {
	
		return ((Movies)obj1).Name.compareTo(((Movies)obj2).Name);
	}
}

class SortByColl implements Comparator {

	public int compare(Object obj1, Object obj2) {
	
		return (int)(((Movies)obj1).Coll - ((Movies)obj2).Coll);
	}
}
class SortByIMDB implements Comparator {

	public int compare(Object obj1, Object obj2) {
	
		return (int)(((Movies)obj1).imdbRating - ((Movies)obj2).imdbRating);
	}
}

class UserListSort {

	public static void main(String[] args) {
	
		ArrayList al = new ArrayList();
		al.add(new Movies("RHTDM",200.00,8.8f));
		al.add(new Movies("Ved",75.00,7.5f));
		al.add(new Movies("Sairat",100.00,8.9f));
		al.add(new Movies("Bajrangi",250.00,7.0f));

		System.out.println(al);

		Collections.sort(al, new SortByName());
		System.out.println(al);
		
		Collections.sort(al, new SortByColl());
		System.out.println(al);
		
		Collections.sort(al, new SortByIMDB());
		System.out.println(al);
	}
}
