//SortedSet

import java.util.*;

class SortedSetDemo {

	public static void main(String[] args) {
	
		SortedSet ss = new TreeSet();
		ss.add("Kanha");
		ss.add("Rajesh");
		ss.add("Rahul");
		ss.add("Ashish");
		ss.add("Badhe");
		

		System.out.println(ss);
		
		//subSet(E,E)
		System.out.println(ss.subSet("Ashish" ,"Rahul"));
		
		//headSet(E)
		System.out.println(ss.headSet("Kanha"));
		
		//tailSet(E)
		System.out.println(ss.tailSet("Kanha"));
		
		//first()
		System.out.println(ss.first());

		//last()
		System.out.println(ss.last());


		System.out.println(ss);
	}
}
