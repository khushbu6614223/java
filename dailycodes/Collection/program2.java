//Arraylist class Methods


import java.util.*;

class ArrayListDemo {

	public static void main(String[] args) {
	
		ArrayList al = new ArrayList();

		//add(E)
		al.add(10);
		al.add(20.5f);
		al.add("Shashi");
		al.add("core2web");
		al.add(10);
		al.add(20.5f);

		System.out.println(al);

		//int size()
		System.out.println(al.size());

		//contains(java.lang.Object)
		System.out.println(al.contains("Shashi"));
		System.out.println(al.contains(30));

		//indexOf(Object)

		System.out.println(al.indexOf(10));
		System.out.println(al.indexOf(20.5f));

		//lastIndexOf(Object)
		System.out.println(al.lastIndexOf(20.5f));

		//get(int)
		System.out.println(al.get(3));
		System.out.println(al.get(5));

		//set(int, E)
		System.out.println(al.set(3,"Incubator"));
		System.out.println(al);
		
		//add(int,E)
		al.add(3,"c2w");
		System.out.println(al);

		//remove(int)
		System.out.println(al.remove(4));
		System.out.println(al);
		
		//new Collection
		ArrayList al2 = new ArrayList();
		al2.add("Bagal");
		al2.add("Biencaps");

		System.out.println(al2);
		
		//addAll(Collection)
		al.addAll(al2);
		System.out.println(al);

		//addAll(int, Collection)
		al.addAll(3,al2);
		System.out.println(al);
	
		//java.lang.Object[] toArray();
		Object arr[] = al.toArray();
		System.out.println(arr);
		for(Object data : arr) {
	
			System.out.println(data + " ");
		
		}

		//clear()
		al.clear();
		System.out.println(al);
	}


}
