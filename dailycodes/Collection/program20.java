//NavigableSet


import java.util.*;

class NavigableSetDemo {

	public static void main(String[] args) {

		NavigableSet ts = new TreeSet();

		ts.add("Kanha");
		ts.add("Ashish");
		ts.add("Badhe");
		ts.add("Rahul");

		System.out.println(ts);

		//descendingSet()
		System.out.println(ts.descendingSet());	
		
		//lower(E) (< E) 
		System.out.println(ts.lower("Kanha"));

		//higher(E) (> E)
		System.out.println(ts.higher("Badhe"));

		//floor(E) (<= E)
		System.out.println(ts.floor("Ashish"));

		//ceiling(E) (>= E)
		System.out.println(ts.ceiling("Shashi"));
		
		//iterator()
		Iterator itr = ts.iterator();
		while(itr.hasNext()) {
		
			System.out.println(itr.next());	
		}

		//descendingIterator()
		Iterator itr1 = ts.descendingIterator();
		while(itr1.hasNext()) {
		
			System.out.println(itr1.next());	
		}
		
		//subSet(E,bool,E,bool)
		System.out.println(ts.subSet("Ashish",false,"Rahul",true));
		
		//headSet(E, bool)
		System.out.println(ts.headSet("Kanha",true));

		//tailSet(E, bool)
		System.out.println(ts.tailSet("Badhe",false));
		
		//subSet(E, E)
		System.out.println(ts.subSet("Ashish","Rahul"));
		
		//headSet(E)
		System.out.println(ts.headSet("Badhe"));

		//tailSet(E)
		System.out.println(ts.tailSet("Kanha"));
		
		//pollFirst()
		System.out.println(ts.pollFirst());

		//pollLast()
		System.out.println(ts.pollLast());

		System.out.println(ts);
	}
}
