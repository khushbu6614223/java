//HashMap

import java.util.*;

class HashMapDemo {

	public static void main(String[] args) {

	HashMap hm = new HashMap();

	hm.put("Kanha","Infosys");
	hm.put("Ashish","BarClays");
	hm.put("Kanha","Carpro");      //duplicate key is updated with latest value
	hm.put("Rahul","BMC");
	hm.put("Badhe","Carpro");     //duplicate values are allowed

	System.out.println(hm);
	
	}
}
