//LinkedHashMap


import java.util.*;

class LinkedHashMapDemo {

	public static void main(String[] args) {
	
		LinkedHashMap hm = new LinkedHashMap();
		
		hm.put("Badhe","Carpro");
		hm.put("Kanha","Infosys");
		hm.put("Ashish","Barclays");
		hm.put("Rahul","BMC");

		System.out.println(hm);
	}
}
