//HashMap Mathods


import java.util.*;

class HashMapMethods {

	public static void main(String[] args) {
	
		HashMap hm = new HashMap();

		hm.put("Java",".java");
		hm.put("Python",".py");
		hm.put("Dart",".dart");

		System.out.println(hm);

		//get(Object);
		System.out.println(hm.get("Python"));

		//keySet():
		System.out.println(hm.keySet());

		//values()
		System.out.println(hm.values());

		//entrySet();
		System.out.println(hm.entrySet());

		//remove(object);
		System.out.println(hm.remove("Java"));
	}
}
