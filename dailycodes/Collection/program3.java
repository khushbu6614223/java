//Arraylist class 


import java.util.*;

class ArrayListDemo extends ArrayList{

	public static void main(String[] args) {
	
		ArrayListDemo al = new ArrayListDemo();

		al.add(10);
		al.add(20.5f);
		al.add("Shashi");
		al.add("core2web");
		al.add(10);
		al.add(20.5f);

		//protected void removeRange(int, int)
		al.removeRange(3,5);
		System.out.println(al);
	}
}
