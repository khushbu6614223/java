//Sorted Map


import java.util.*;

class SortedMapDemo {

	public static void main(String[] args) {

		SortedMap tm = new TreeMap();

		tm.put("Ind","India");
		tm.put("Pak","Pakistan");
		tm.put("SL","SriLanka");
		tm.put("Aus","Australia");
		tm.put("Ban","Bangladesh");

		System.out.println(tm);

		//subMap(K,K)

		System.out.println(tm.subMap("Aus","Pak"));
		
		//headMap(K)
		System.out.println(tm.headMap("Ind"));

		//tailMap(K)
		System.out.println(tm.tailMap("Ind"));

		//firstKey()
		System.out.println(tm.firstKey());

		//lastKey()
		System.out.println(tm.lastKey());

		//keySet()
		System.out.println(tm.keySet());

		//values()
		System.out.println(tm.values());

		//entrySet()
		System.out.println(tm.entrySet());
	}

}
