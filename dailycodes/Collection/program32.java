//Navigablemap Methods


import java.util.*;

class NavigableMapDemo {

	public static void main(String[] args) {
	
		NavigableMap tm = new TreeMap();
	
		tm.put("Ind","India");
		tm.put("Pak","Pakistan");
		tm.put("SL","SriLanka");
		tm.put("Aus","Australia");
		tm.put("Ban","Bangladesh");

		System.out.println(tm);
		
		//lowerEntry(K); (strictly less than K)
		System.out.println(tm.lowerEntry("Ind"));

		//higherEntry(K) (strictly greater than K)
		System.out.println(tm.higherEntry("Pak"));

		//floorEntry(K); (less thsn or equal K)
		System.out.println(tm.floorEntry("USA"));

		//ceilingEntry(K) (greater than or equal K)
		System.out.println(tm.ceilingEntry("Aus"));


		//lowerKey(K); (strictly less than K)
		System.out.println(tm.lowerKey("Ind"));

		//higherKey(K) (strictly greater than K)
		System.out.println(tm.higherKey("Pak"));

		//floorKey(K); (less thsn or equal K)
		System.out.println(tm.floorKey("USA"));

		//ceilingKey(K) (greater than or equal K)
		System.out.println(tm.ceilingKey("Aus"));

		//firstEntry();
		System.out.println(tm.firstEntry());

  		//lastEntry();
		System.out.println(tm.lastEntry());
  
		//descendingMap();
		System.out.println(tm.descendingMap());
  
		//navigableKeySet();
		System.out.println(tm.navigableKeySet());
  
		//descendingKeySet();
		System.out.println(tm.descendingKeySet());
 
		// subMap(K, boolean, K, boolean);
		System.out.println(tm.subMap("Aus",true,"Pak",false));
		
		// headMap(K, boolean);
		System.out.println(tm.headMap("Ind",false));
		
		// tailMap(K, boolean);
		System.out.println(tm.tailMap("Ind",true));
  		
		// subMap(K, K);
		System.out.println(tm.subMap("Aus","Ind"));
  		
		// headMap(K);
		System.out.println(tm.headMap("Pak"));
  		
		// tailMap(K);
		System.out.println(tm.tailMap("Ind"));
		
		// pollFirstEntry();
		System.out.println(tm.pollFirstEntry());
  
		// pollLastEntry();
		System.out.println(tm.pollLastEntry());
	}
}
