//Priority Queue
//Comparable

import java.util.*;
class Project implements Comparable {

	String projName = null;
	int teamSize = 0;
	int duration = 0 ;

	Project(String projName, int teamSize, int duration) {
	
		this.projName = projName;
		this.teamSize = teamSize;
		this.duration = duration;

	}

	public String toString() {

		return "{" + projName + ";" + teamSize + ";" + duration + " days" + "}";
	}

	public int compareTo(Object obj) {
	
		return ((Project)obj).projName.compareTo(((Project)obj).projName);
	}
}

class PQueueDemo {

	public static void main(String[] args) {
	
		PriorityQueue pq = new PriorityQueue();

		pq.offer(new Project("Java",100,160));
		pq.offer(new Project("Webapp",15,75));
		pq.offer(new Project("Python",20,90));

		System.out.println(pq);
	}
}
