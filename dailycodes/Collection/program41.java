//PriorityQueue
//Comparator

import java.util.*;
class Project {

	String projName = null;
	int teamSize = 0;
	int duration = 0 ;

	Project(String projName, int teamSize, int duration) {
	
		this.projName = projName;
		this.teamSize = teamSize;
		this.duration = duration;

	}

	public String toString() {
		return "{" + projName + ";" + teamSize + ";" + duration + " days" + "}";
	}

}
class SortByName implements Comparator {

	public int compare(Object obj1 , Object obj2) {
	
		return ((Project)obj1).projName.compareTo(((Project)obj2).projName);
	}
}

class PQueueDemo {

	public static void main(String[] args) {
	
		PriorityQueue pq = new PriorityQueue(new SortByName());

		pq.offer(new Project("Java",100,160));
		pq.offer(new Project("c",15,75));
		pq.offer(new Project("Python",20,90));

		System.out.println(pq);
	}
}
