//BlockingQueue

import java.util.*;
import java.util.concurrent.*;

class BlockingQueueDemo {

	public static void main(String[] args) throws InterruptedException{
	
		BlockingQueue bQueue = new ArrayBlockingQueue(3);

		//put(E)
		bQueue.put(10);
		bQueue.put(20);
		bQueue.put(30);
		System.out.println(bQueue);

		//offer(E,long,Timeunit)
		bQueue.offer(40,5,TimeUnit.SECONDS);
		System.out.println(bQueue);

		//take()
		bQueue.take();
		System.out.println(bQueue);

		ArrayList al = new ArrayList();
		System.out.println("ArrayList" + al);
		//drainTo(collection)
		bQueue.drainTo(al);
		System.out.println("ArrayList" + al);
		System.out.println(bQueue);

	}
}
