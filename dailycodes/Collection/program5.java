//Linkedlist class Methods

import java.util.*;

class LinkedListDemo {

	public static void main(String[] args) {
	
		LinkedList ll = new LinkedList();
		
		//add(E)
		ll.add(10);
		ll.add(30);
		
		//addFirst(E)
		ll.addFirst(20);

		//addLast(E)
		ll.addLast(40);

		//add(int,E)
		ll.add(2,25);

		System.out.println(ll);
	}
}
