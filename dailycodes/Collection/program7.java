//Iterator and ListIterator

import java.util.*;

class CursorDemo{

	public static void main(String[] args) {
	
		ArrayList al = new ArrayList();
		al.add("Ashish");
		al.add("Kanha");
		al.add("Rahul");
		al.add("Badhe");

		//iterator
		Iterator cursor = al.iterator();
		while(cursor.hasNext()) {
		
			if("Kanha".equals(cursor.next()))
				cursor.remove();
		}
		System.out.println(al);

		//ListIterator
		ListIterator litr = al.listIterator();
		while(litr.hasNext()) {
		
			if("Rahul".equals(litr.next()))
				litr.remove();
		}
		System.out.println(al);

		while(litr.hasPrevious()) {
		
			if("Rahul".equals(litr.previous()))
				litr.remove();
		}
	}
}
