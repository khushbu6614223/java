//vector class and Enumeration cursor


import java.util.*;

class VectorDemo {

	public static void main(String[] args) {
	
		Vector v = new Vector();

		v.add(10);
		v.addElement(20);
		v.addElement(30);
		v.addElement(40);
		v.addElement(50);
		v.addElement(10);
		
		System.out.println(v);

		Enumeration cursor = v.elements();
		System.out.println(cursor.getClass().getName());

		while(cursor.hasMoreElements()){
		
			System.out.println(cursor.nextElement());
		}

	}
}
