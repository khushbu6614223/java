//Stack

import java.util.*;

class StackDemo {

	public static void main(String[] args) {
	
		Stack s = new Stack();

		//push(E)
		s.push(10);
		s.push(20);
		s.push(30);
		s.push(40);

		System.out.println(s);
		
		//pop();
		s.pop();
		System.out.println(s);
		
		//peek();
		System.out.println(s.peek());
		
		//search(Object);
		System.out.println(s.search(20));

		//empty();
		System.out.println(s.empty());
	}
}
