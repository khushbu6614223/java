//WAP of Number format exception handling


import java.io.*;

class ExceptionDemo{

	public static void main(String[] args)throws IOException{
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		String str = br.readLine();
		System.out.println(str);
		int data = 0; 
		try{
			data = Integer.parseInt(br.readLine());
		}catch(NumberFormatException ne){

			System.out.println("Please Enter integer Data");
		}
		System.out.println(data);
	}
}

