//WAP of ArrayIndexOutOfBounds Exception handling


class ArrayException{

	public static void main(String[] args){
	
		int arr[] = new int[]{10,20,30,40,50};
		for(int i = 0; i<=arr.length; i++){
			try{
				System.out.println(arr[i]);
			}catch(ArrayIndexOutOfBoundsException ai){
			
				System.out.println("Exception Handled");
			}
		}
	}

}
