//WAP of NullPointerException handling


class NullException{
	void m1(){
	
		System.out.println("In m1");
	}
	void m2(){
	
		System.out.println("In m2");
	}

	public static void main(String[] args){
	
		System.out.println("Start main");
		NullException obj = new NullException();
		obj.m1();
		obj = null;
		try{
			obj.m2();
		}catch(NullPointerException npe){
			System.out.println("Exception Handled");
		}
		System.out.println("End main");
	}
}
