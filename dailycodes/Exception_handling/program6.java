//WAP of InterruptedException Handling


class Demo{

	public static void main(String[] args) { // throws InterruptedException {
	
		for(int i = 0; i<10; i++){
		
			System.out.println("In Loop");
			try{
				Thread.sleep(5000);
			}catch(InterruptedException ie) {
			
				System.out.println("Exception handled");
			}
		}
	}
}
