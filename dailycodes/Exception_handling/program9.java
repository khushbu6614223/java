//WAP of User-defined Exception handling

import java.util.*;
import java.io.*;

class DataOverFlowException extends RuntimeException {

	DataOverFlowException(String msg) {
	
		super(msg);
	}
	
}

class DataUnderFlowException extends RuntimeException {

	DataUnderFlowException(String msg) {
	
		super(msg);
	}
}
class ArrayDemo {

	public static void main(String[] args) {
	
		int arr[] = new int[5];
		Scanner sc = new Scanner(System.in);
		
		System.out.println("Enter integer value");
		System.out.println("Note : 0<element<100");
		
		for(int i=0; i<arr.length; i++) {

			int data = sc.nextInt();

			if(data<0) {
				
				throw new DataUnderFlowException("data is less than Zero");
			}
			if(data<0) {
				
				throw new DataOverFlowException("data is more than Zero"); 
			}
				arr[i] = data;
		}
		for(int i=0; i<arr.length; i++) {
		
			System.out.println(arr[i] + " ");
		}
	}

}
