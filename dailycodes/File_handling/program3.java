//File class methods

import java.io.*;
class Demo {

	public static void main(String[] args) throws IOException {
	
		File fobj1 = new File("Incubator.txt");
		fobj1.createNewFile();

		System.out.println(fobj1.getName());
		System.out.println(fobj1.getParent());
		
		System.out.println(fobj1.getPath());
		System.out.println(fobj1.getAbsolutePath());

		System.out.println(fobj1.canRead());
		System.out.println(fobj1.canWrite());
		
		System.out.println(fobj1.isDirectory());
		System.out.println(fobj1.isFile());

		int count = 0;
		File fobj2 = new File("/home/khushbu/java_dsa/java/Daily_codes/File_handling");
		System.out.println(fobj2.exists());
		String[] files = fobj2.list();

		for(String str : files) {
		
			System.out.println(str);
			File f = new File(str);
			if(f.isFile()) {
		
				count++;
			}
		}
		System.out.println(count);
	}
}
