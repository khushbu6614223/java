import java.util.*;
import java.io.*;
class IODemo{
	public static void main(String[] args)throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("enter student name,div,marks,percent");
		String info =br.readLine();

		System.out.println(info);
		StringTokenizer obj=new StringTokenizer(info, " ");

		String token1=obj.nextToken();
		char token2=((char)obj.nextToken());
		int token3=Integer.parseInt(obj.nextToken());
		float token4=Float.parseFloat(obj.nextToken());

		System.out.println("student name= "+token1);
		System.out.println("div= "+token2);
		System.out.println("marks= "+token3);
		System.out.println("percent= "+token4);
	}
}
