//take an integer as input and print whether it is divisible by 4 or not

class Demo{

        public static void main(String[] args){

                int x = 25;

                if(x%4 == 0){

                        System.out.println(x +" is divisible by 4");
                }
            
                else{

                        System.out.println(x + " is not divisible by 4");
                }
        }
}
