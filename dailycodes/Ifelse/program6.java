/*Given an integer input A which represents units of electricity consumed at your house
   	  claculate and print the bill amount
	  units <= 100 : price per unit is 1
 	  units > 100  : price per unit is 2
*/

class Demo{

	public static void main(String[] args){
	
		int units = 550;
		if(units<=100){
		
			System.out.println(units*1);
		}
		else{
		
			System.out.println(100*1 + (units -100)*2);
		}

	}
}

