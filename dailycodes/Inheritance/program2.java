//Instance method


class Parent {

	Parent() {
	
		System.out.println("In parent Contructor");
	}
	void parentProperty() {
	
		System.out.println("Flat,Gold,Car");
	}
}
class Child extends Parent {

	Child() {
	
		System.out.println("In Child Constructor");
	}
}
class Client {

	public static void main(String[] args) {
	
		Child obj = new Child();
		obj.parentProperty();
	}
}
