//WAP to implement normal Inner class (by creating seperate object of outer and inner class)

class Outer{

	class Inner{
		
		void fun2(){

			System.out.println("In Inner");
		}
	}
	
	void fun1(){
		
		System.out.println("In Outer");
	}
}
class Client{

	public static void main(String[] args){
	
		Outer obj = new Outer();
		obj.fun1();

		Outer.Inner obj1 = obj.new Inner();
		obj1.fun2();
	}
}
