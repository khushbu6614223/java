//WAP for normal Inner class(without creating seperate object of outer class)

class Outer{

	class Inner{
	
		void fun2(){
		
			System.out.println("In Inner");
		}
	}
	void fun1(){
	
		System.out.println("In Outer");
	}
}
class Client{

	public static void main(String[] args){
	
		Outer.Inner obj = new Outer().new Inner();

		obj.fun2();
	}
}
