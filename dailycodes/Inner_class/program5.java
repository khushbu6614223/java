//WAP of normal Inner class to print static variable of inner class


class Outer{
	
	int x = 10;
	static int y =20;

	class Inner{
		int a = 10;
		final static int b = 20;

	}
}
class Client{

	public static void main(String[] arsg){
	
		Outer.Inner obj = new Outer().new Inner();
                System.out.println(Outer.Inner.b);
		System.out.println(obj.a);
		System.out.println(obj.b);
	}
}
