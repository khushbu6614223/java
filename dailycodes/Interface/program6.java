//static keyword (child class cannot override method)

interface Demo1 {

	static void fun() {
	
		System.out.println("In fun Demo1");
	}
}

interface Demo2 {

	static void fun() {
	
		System.out.println("In fun Demo2");
	}
}

class DemoChild implements Demo {

	void fun() {
	
		System.out.println("In child fun");
		Demo1.fun();
		Demo2.fun();
	}
}

class Client {

	public static void main(String[] args) {
	
		DemoChild obj = new DemoChild();
		obj.fun();
	}

}
