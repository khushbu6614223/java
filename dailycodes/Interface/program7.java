//Variables in Interface

interface Demo {

	int x = 10;
	static int y = 20;
	void fun();
}

class DemoChild implements Demo {

	public void fun() {
	
		System.out.println("In DemoChild fun");
		System.out.println(x);
		System.out.println(y);
	}
	
}
class Client {

	public static void main(String[] args) {
	
		Demo obj = new DemoChild();
		obj.fun();
		System.out.println(Demo.x);
		System.out.println(Demo.y);
	}
}
