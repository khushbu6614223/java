//Runnable interface with Lambda Function
//


class ThreadDemo  {

	public static void main(String[] args) {
	
		Runnable obj1 = ()-> {

				System.out.println(Thread.currentThread().getName());
		};

		Thread t1 = new Thread(obj1);
		t1.start();
	}
}
