//WAP of MultiThreading for Thread class and implement methods of thread class


class MyThread implements Runnable {

	public void run() {
	
		System.out.println(Thread.currentThread());
	}
}
class ThreadGroupDemo{

	public static void main(String[] args){

		ThreadGroup pThreadGP = new ThreadGroup("India");

		MyThread obj1 = new MyThread();
		MyThread obj2 = new MyThread();

		Thread t1 = new Thread(pThreadGP,obj1, "Maha");
		Thread t2 = new Thread(pThreadGP,obj2, "Goa");

		t1.start();
		t2.start();
		
		ThreadGroup cThreadGP = new ThreadGroup("Bangladesh");

		Thread t3 = new Thread(cThreadGP,obj1, "Dhaka");
		Thread t4 = new Thread(cThreadGP,obj2, "Mirpur");

		t3.start();
		t4.start();

		pThreadGP.interrupt();
		System.out.println(pThreadGP.activeCount());
		System.out.println(pThreadGP.activeGroupCount());
	}
}
