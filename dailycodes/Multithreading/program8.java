
//WAP of concurrency method(yield method) of thread class

class MyThread extends Thread{

	public void run() {
		
		for(int i = 0; i<6; i++){
		
			System.out.println("In thread-0");
		}
			System.out.println(Thread.currentThread());
	}
}
class ThreadDemo{

	public static void main(String[] args){

		MyThread obj = new MyThread();
		obj.start();
		obj.yield();

		for(int i = 0; i<6; i++){
		
			System.out.println("In main");
		}
		System.out.println(Thread.currentThread());
	}
}
