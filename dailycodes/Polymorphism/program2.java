//overriding

class Parent {

	Parent() {
	
		System.out.println("Parent-constructor");
	}
	void property() {
	
		System.out.println("Flat,Gold,Car");
	}
	void marry() {
	
		System.out.println("Deepika");
	}
}
class Child extends Parent {

	Child() {
	
		System.out.println("Child-constructor");
	}
	void marry() {
	
		System.out.println("Alia");
	}
}
class Client {

	public static void main(String[] args) {
	
		Child obj = new Child();
		obj.property();
		obj.marry();
	}
}
