//WAP to access the size of array from array and enter the elemnts from user
//print the count of even elements
//by scanner class

import java.util.*;

class ECountDemo{

        public static void main(String args[]){

                Scanner sc= new Scanner(System.in);

                System.out.println("Enter the size of array:");
                int size= sc.nextInt();
                int arr[]=new int[size];

                System.out.println("Enter the elements of array:");
                for(int i=0; i<arr.length; i++){
                        arr[i]=sc.nextInt();
                }
                int ecount=0;
                for(int i=0; i<arr.length; i++){
                        if(arr[i]%2==0){
                        ecount++;
                        }
                }
        System.out.println("Evencount= " +ecount);
        }
}
