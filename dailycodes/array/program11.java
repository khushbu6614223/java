//WAP to access the size of array from array and enter the elemnts from user
//print the count of even and odd elements
//by bufferedReader

import java.io.*;

class ECountDemo{

        public static void main(String args[])throws IOException{

                BufferedReader br= new BufferedReader(new InputStreamReader(System.in));

                System.out.println("Enter the size of array:");
                int size=Integer.parseInt(br.readLine());
                int arr[]=new int[size];

                System.out.println("Enter the elements of array:");
                for(int i=0; i<arr.length; i++){
                        arr[i]=Integer.parseInt(br.readLine());
                }
                int ecount=0;
		int ocount=0;
                for(int i=0; i<arr.length; i++){
                        if(arr[i]%2==0)
                        ecount++;
                        
			else
				ocount++;
                }
        System.out.println("Evencount= " +ecount);
        System.out.println("oddcount= " +ocount); 
	}
}
