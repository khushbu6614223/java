//Implementation of array addresses and identityhashcode


class ArrayDemo{

	public static void main(String[] args){
	
		int arr[] = new int[]{10,20,30};
		int arr1[] = new int[]{10,20,30};

		System.out.println(arr);
		System.out.println(arr1);

		System.out.println(System.identityHashCode(arr[0]));
                System.out.println(System.identityHashCode(arr1[0]));		

	        System.out.println(System.identityHashCode(arr));
		System.out.println(System.identityHashCode(arr1));
	}
}
