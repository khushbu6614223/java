//Implementation of array internal of different data type


class ArrayDemo{

	public static void main(String args[]){
	
		int arr1[] = {10,20,30};
		char arr2[] = {'a','b','c','d','e'};
		float arr3[] = {10f,20.5f,30f};
		byte arr4[] = {1,2,3,4,5};
		boolean arr5[] = {true, false,true};
		short arr6[] = {10,20,30};
		double arr7[] = {10,20,30};
		//float arr8[] = {10.23,20.4};

                System.out.println(System.identityHashCode(arr1));
                System.out.println(System.identityHashCode(arr2));
                System.out.println(System.identityHashCode(arr3));
                System.out.println(System.identityHashCode(arr4));
                System.out.println(System.identityHashCode(arr5));
                System.out.println(System.identityHashCode(arr6));
                System.out.println(System.identityHashCode(arr7));
                //System.out.println(System.identityHashCode(arr8));
		
		System.out.println(arr1);
                System.out.println(arr2);
                System.out.println(arr3);
                System.out.println(arr4);
                System.out.println(arr5);
                System.out.println(arr6);
                System.out.println(arr7);		
	}
}
