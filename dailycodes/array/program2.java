//WAP to print the arrays of different data types 

class Demo{
	public static void main(String[] args){

		int arr[] = {0,7,9,8,7};
	
		char arr1[] = {'a','b','c'};

		float arr3[] = {10.6f,7.0f};

		boolean arr4[] = {true,false,true};

             System.out.println(arr[0]);
             System.out.println(arr[1]);
             System.out.println(arr[2]);
             System.out.println(arr[3]);
             System.out.println(arr[4]);                  

             System.out.println(arr1[0]);
             System.out.println(arr1[1]);               
             System.out.println(arr1[2]);

             System.out.println(arr3[0]);
	     System.out.println(arr3[1]);


	     System.out.println(arr4[0]);
	     System.out.println(arr4[1]);
             System.out.println(arr4[2]);    
	}
}
