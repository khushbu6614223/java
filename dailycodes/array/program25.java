//Implementation of jagged Araay

class JaggedArrayDemo{

	public static void main(String args[]){

		int arr1[][] = {{1,2,3},{4,5},{6}};

		int arr2[][] = new int[3][];
		arr2[0] = new int[]{1,2,3};
	        arr2[1] = new int[]{4,5};
                arr2[2] = new int[]{6};	
		
		System.out.println(arr1[0][0]);
                System.out.println(arr1[1]);
                System.out.println(arr1[2]);

                System.out.println(arr2[0]);
                System.out.println(arr2[1]);
                System.out.println(arr2[2]);		

	}
}
