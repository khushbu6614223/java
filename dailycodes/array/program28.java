//Implementation of jagged array
//accessing elemnets from user

import java.io.*;
class JaggedArrayDemo{

        public static void main(String[] args)throws IOException{

                BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
 		System.out.println("enter the size of array");
		int size = Integer.parseInt(br.readLine());
 		int arr[][] = new int[size][];
                            
                System.out.println("enter the elements");
                for(int i=0; i<arr.length; i++){
                        for(int j=0; j<arr[i].length; j++){
                                arr[i][j] = Integer.parseInt(br.readLine());
                        }
                }
                 for(int i=0; i<arr.length; i++){
                        for(int j=0; j<arr[i].length; j++){
                                System.out.print(arr[i][j] + " ");
                        }
                        System.out.println();
                 }

                for(int[] x : arr){
                        for(int y : x){
                                System.out.print(y + " ");
                        }
                        System.out.println();
                }

        }

}
