//Implementation of 2Darray 
//print elements using for loop


class TwoDarrayDemo{

        public static void main(String[] args){

                char arr[][]=new char[2][3];
                arr[0][0]='a';
                arr[0][1]='s';
                arr[1][0]='s';
                arr[1][1]='d';

                for(int i=0; i<2; i++){
                        for(int j=0; j<3; j++){
                                System.out.print(arr[i][j]+" ");
                        }
                        System.out.println(" ");
		}
	}
       
}
