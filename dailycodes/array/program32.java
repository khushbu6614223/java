//Implementation of 2Darray

class TwoDarrayDemo{

        public static void main(String[] args){

                char arr[][]=new char[2][3];
                arr[0][0]='a';
                arr[0][1]='s';
                arr[1][0]='s';
                arr[1][1]='d';

                System.out.println(arr[0][1]);
                System.out.println(arr[0]);
                System.out.println(arr[1]);
                System.out.println(arr);

                System.out.println(arr.length);
        }
}
