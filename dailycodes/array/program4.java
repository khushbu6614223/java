//WAP to take the elements of array from user and print using for loop
//by scanner class


import java.util.*;
class InputDemo{

	public static void main(String[] args){
	
		Scanner sc =new Scanner(System.in);
		String arr[] = new String[4];

		System.out.println("Enter elements");
		arr[0]=sc.next();
		arr[1]=sc.next();
		arr[2]=sc.next();
		arr[3]=sc.next();

		System.out.println("Output:");
		for(int i=0; i<4; i++){
			System.out.println(arr[i]);
		}
	}
}
