//WAP to take the elements of array from user and print using for loop
//by bufferedreader method

import java.io.*;

class Demo{

	public static void main(String[] args)throws IOException{

		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
		char arr[] =new char[4];
		
		System.out.println("Enter characters:");
		for(int i=0; i<4; i++){
			arr[i]=(char)br.read();
			br.skip(1);
		}
		System.out.println("Output:");
		for(int i=0; i<4; i++){
			System.out.println(arr[i]);
		}



	}
}
