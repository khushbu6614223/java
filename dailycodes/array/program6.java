//WAP to take the elements of array from user and print using for loop
//by bufferedreader method

import java.io.*;

class Demo{
	public static void main(String[] args)throws IOException{
	
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
		float arr[]=new float[4];

		System.out.println("Enter elements:");
		arr[0]=Float.parseFloat(br.readLine());
		arr[1]=Float.parseFloat(br.readLine());
		arr[2]=Float.parseFloat(br.readLine());
		arr[3]=Float.parseFloat(br.readLine());

		System.out.println("Output");
		for(int x=0; x<4; x++){
			System.out.println(arr[x]);
		}
	}
}
