//WAP to access the size of array from array and enter the elemnts from user
//by scanner class

import java.util.*;

class Demo{

	public static void main(String[] args){

		Scanner sc= new Scanner(System.in);

		System.out.println("Enter the size of Array:");
		int size=sc.nextInt();
		int arr[]=new int[size];
		System.out.println("Enter the elements:");
		for(int i=0; i<arr.length; i++){
			arr[i]=sc.nextInt();
	
		}
		System.out.println("Output:");
		for(int i=0; i<arr.length; i++){
			System.out.println(arr[i]);
		}
	}

}
