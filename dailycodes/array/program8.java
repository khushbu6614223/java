//WAP to access the size of array from array and enter the elemnts from user
//by bufferedreader method

import java.io.*;

class Demo{

	public static void main(String args[])throws IOException{
	
		BufferedReader br= new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter the size of array:");
		int size=Integer.parseInt(br.readLine());
		boolean arr[]=new boolean[size];

		System.out.println("Enter the elements of array:");
		for(int i=0; i<arr.length; i++){
			arr[i]=Boolean.parseBoolean(br.readLine());
		}
			
		System.out.println("output:");
		for(int i=0; i<arr.length; i++){
			System.out.println(arr[i]);
		}			
	}
}
