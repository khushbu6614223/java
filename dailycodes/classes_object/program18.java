//Implementation of static block basic code

class Demo{

	static{
		System.out.println("Static block");
	}
	public static void main(String[] args){
	
		System.out.println("Main method");
	}
}
