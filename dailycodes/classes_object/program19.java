//Implementation of static block


class Demo{

	int x = 10;
	static int y = 20;

	static{
	
		System.out.println("static block1");
	}
	public static void main(String[] arsg){
	
		System.out.println("main method");

		Demo obj = new Demo();
                System.out.println(obj.x);                
		System.out.println(Demo.y);
	}
	static{

		System.out.println("static block2");
                System.out.println(y);
	}
}
