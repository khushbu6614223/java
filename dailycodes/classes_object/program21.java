class Demo{

	static{
                System.out.println("static block1");		
	}
	public static void main(String[] args){
	
                System.out.println("In Demo main method");		
	}
}
class Client{

	static{
                System.out.println("static block2");	
	}
	public static void main(String[] args){

                System.out.println("In client main method");
		Demo obj = new Demo();
        }
	static{
                System.out.println("static block3");
        }
}
