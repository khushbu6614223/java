//Implementation of hidden this reference


class Demo{

	int x = 10;
	Demo(){
	
		System.out.println("In no-args constructor");
		System.out.println("x = "+x);
	}
	Demo(int x){
	
		System.out.println("In para constructor");
		System.out.println("x = "+x);
	}
	public static void main(String[] args){
	
		Demo obj1 = new Demo();
		Demo obj2 = new Demo(20);

	}

}
