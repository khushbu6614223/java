//Implementation of hidden-this reference
//for calling the constructor of self class by making object of only one constructor 

class Demo{

	int x = 10;

	Demo(){
		
		System.out.println("In no-args Constructor");
	}
	Demo(int x){
		
		this();
		System.out.println("In para Constructor");
		
	}
	public static void main(String[] args){
	
		Demo obj2 = new Demo(50);
	}
}
