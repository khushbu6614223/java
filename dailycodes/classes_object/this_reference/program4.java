//Implementation of hidden this reference


class Player{

	private int jerNo = 0;
	private String name = null;
	
	Player(int jerNo, String name){
	
		this.jerNo = jerNo;
		this.name = name;

		System.out.println("In no-args Constructor");
	}
	void Info(){
		System.out.println(jerNo + " = " + name);
	}
}

class Client{

	public static void main(String[] args){

		Player obj1 = new Player(18,"virat");
		obj1.Info();

		Player obj2 = new Player(7,"MSD");
		obj2.Info();

		Player obj3 = new Player(45,"Rohit");
		obj3.Info();
		
	}
}
