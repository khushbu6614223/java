//bitwise left and right shift operator
//[<< >>]
class Demo{

        public static void main(String args[]){

                int x = 8;
                int y = 10;

                System.out.println(x<<2);
                System.out.println(y>>2);
        }
}
