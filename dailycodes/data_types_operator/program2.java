//initialization of all data type


class Demo{

	public static void main(String[] args){
	
		int var1 = 20;				//4 byte

		float var2 = 10.5f;			//4 byte
		float var3 = 10f;			

		double var4 = 100;			//8 byte
		double var5 = 10.5;
		
		char var6 = 'A';			//2 byte
		char var7 = 'a';
		
		long var8 = 123457978;			//8 byte
		
		short var9 = 6;				//2 byte
		
		boolean var10 = true;			//1 bit
		
		byte var11 = 121;			//1 byte

		System.out.println(var1);
		System.out.println(var2);
		System.out.println(var3);
		System.out.println(var4);
		System.out.println(var5);
		System.out.println(var6);
		System.out.println(var7);
		System.out.println(var8);
		System.out.println(var9);
		System.out.println(var10);
		System.out.println(var11);
	}
}
