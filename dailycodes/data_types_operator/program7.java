//Unary operator
//(++ --)

class Demo{

	public static void main(String[] args){
	
		int x = 5;
		int y = 6;
		int z = 7;
		int w = 8;

		System.out.println(z);

		System.out.println(++x);

		System.out.println(y++);

		System.out.println(--z);

		System.out.println(w--);

	}
}
