//Annotation
//@Override


class Parent {

	void m1() {
	
		System.out.println("In parent-m1");
	}
}
class Child extends Parent {

	//@Override	  	//after removing comment compiler will tell us that the scenario is of overriding or not(as code is not of overriding)
	void m1(int x) {
	
		System.out.println("In child-m1");
	}
}
class Client {

	public static void main(String[] args) {
	
		Child obj1 = new Child();
		obj1.m1();

		Parent obj2 = new Parent();
		obj2.m1();
		
		Parent obj3 = new Child();
		obj3.m1();
	}
}
