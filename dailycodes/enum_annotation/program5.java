//Annotation
//@Deprecated
//@SuppressWarnings


class Parent {

	@Deprecated
	void m1() {
	
		System.out.println("Parent- m1");
	}
}
class Client {

	public static  void main(String[] args) {
	
		@SuppressWarnings("Hello")
		Parent p  = new Parent();
		p.m1();
	}
}
