// print odd integers from 1 to N


class Demo {

	public static void main(String[] args) {

		int N = 6;
		for(int i=1; i<=N; i++) {
		
			if(i%2 != 0) {
			
				System.out.println(i);
			}
		}
	}
}
