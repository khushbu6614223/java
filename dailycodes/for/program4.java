//Take N as input Print its factors factor
//x is a factor of N if N%x==0


class Demo {

	public static void main(String[] args) {
	
		int N = 6;
		for(int i=1; i<=N; i++) {
		
			if(N%i == 0) {
			
				System.out.println(i);
			}
		}
	}
}
