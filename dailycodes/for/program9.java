//1 to 50 divisible by 3 & 5 or 4 then skip


class Demo {

	public static void main(String[] args) {
	
		for(int i=1; i<=50; i++) {
		
			if((i%3==0 && i%5==0) || i%4==0) {
			
				continue;
			}	
			System.out.println(i);
		}
	}
}
