/*print
 A 1 B 2
 C 3 D
 E 4 
 F*/

class Pattern {

	public static void main(String[] args) {
	
		char ch = 'A';
		int num = 1;
		int row = 4;

		for(int i=1; i<=row; i++) {
		
			for(int j=1; j<=row-i+1; j++) {
			
				if(j%2 == 1) {

					System.out.print(ch++ +" ");
				}
				else {
					System.out.print(num++ +" ");
				}
			}
			System.out.println();
		}
	}
}
