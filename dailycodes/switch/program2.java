

class SwitchDemo {

	public static void main(String[] args) {
	
		int ch = 65;
		switch(ch) {
		
			case 'A':
				System.out.println("char-A");
				break;
			case 'B':
				System.out.println("char-B");
				break;
			case 'C':
				System.out.println("char-C");
				break;
			case 'D':
				System.out.println("char-D");
				break;
			default:
				System.out.println("No match");
				break;
		}
		System.out.println("After switch");
	}
}
