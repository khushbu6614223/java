//Given an integer N
//print sum of its digits


class Demo{

	public static void main(String[] args){
	
		int N=6531;
		int sum=0;

		while(N!=0){
			int rem=N%10;
		        sum=rem+sum;
			N=N/10;
		}
		System.out.println("sum of its digit= "+sum);
	}
}
